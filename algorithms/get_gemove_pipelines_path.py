# -*- coding: utf-8 -*-

"""
***************************************************************************
    get_gemove_pipelines_path.py
    -------------------------
    begin                : August 2018
    copyright            : (C) 2018 by Luigi Pirelli
    email                : luipir at gmail dot com
    dev for              : https://cartolab.udc.es
    Project              : https://cartolab.udc.es/geomove
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

__author__ = 'Luigi Pirelli'
__date__ = 'October 2018'
__copyright__ = '(C) 2018, Luigi Pirelli'


import os
from qgis.core import QgsProcessingOutputString
from ..geomove_tools_algorithm import GeoMoveAlgorithm


class GetGeomovePipelinesPath(GeoMoveAlgorithm):
    """
    This utility returns a path string with the location of piplines
    installed with the Geomove provider plugin
    """

    OUTPUT = 'OUTPUT'

    def createInstance(self):
        self.messageTag = type(self).__name__ # e.g. string GetGeomovePipelinesPath
        return GetGeomovePipelinesPath()

    def name(self):
        return 'getgeomovepipelinespath'

    def displayName(self):
        return self.tr('Get Geomove Pipelines Path')

    def group(self):
        return self.tr('Utilities')

    def groupId(self):
        return 'utilities'

    def initAlgorithm(self, config=None):
        self.addOutput(
            QgsProcessingOutputString(
                name=self.OUTPUT,
                description=self.tr('Pipelines path')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        currentPath = os.path.dirname(__file__)
        pipelinesPath = os.path.join(currentPath, '..', 'pdal_pipelines')
        pipelinesPath += os.sep

        # Return the results of the algorithm.
        return {self.OUTPUT: pipelinesPath}
