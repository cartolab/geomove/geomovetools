QGIS Processing provider for GeoMove algorithms integration
===

Description
---

The intent of this provider is to offer a simple way to install all GeoMove models and scripts inside the QGIS Processing Framework.

The plugin was developed by:

* David Fernandez Arango (davidfernandezarango AT hotmail DOT com)
* Luigi Pirelli (luipir AT gmail DOT com)

dev for  : [CartoLab](https://cartolab.udc.es)

Project  : [GeoMove](https://cartolab.udc.es/geomove)

Premise
---
Please read [PDAL](https://pdal.io/) documentation first to understand how to interprete a json pipeline .

Dependencies
----
This provider depends on [pdaltools](https://plugins.qgis.org/plugins/pdaltools/) processing provider that can be installed using QGIS plugin manager.
